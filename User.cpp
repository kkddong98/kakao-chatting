#include "User.h"

namespace Study_1
{
    User::User(char* inputName, char* inputID)
    {
        //유저 이름 저장-> 힙 메모리에 이름 저장
        UserName = new char[strlen(inputName)+1];
        strcpy(UserName, inputName);
        UserName[strlen(inputName)] = '\0';

        //유저 id 저장 ->힙 메모리에 저장
        UserID = new char[strlen(inputID)+1];
        strcpy(UserID, inputID);
        UserName[strlen(inputID)] = '\0';
    }

    User::~User()
    {
        delete[] UserName;
        delete[] UserID;
    }
    
    void User::ChangeUserName(User* NewName)
    {
        delete[] UserName;
        //유저 이름 저장-> 힙 메모리에 이름 저장
        UserName = new char[strlen(NewName)+1];
        strcpy(UserName, NewName);
        UserName[strlen(NewName)] = '\0';
    }

    char* User::GetUserID() const
    {
        return UserID;
    }

    char* User::GetUserName() const
    {
        return UserName;
    }

}
