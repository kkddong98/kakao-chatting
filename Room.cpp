#include"Room.h"

namespace Study_1
{
    Room::Room(unsigned int size)
    :RoomSize(size)
    {
        mUserList = new mUserList[RoomSize-1];
        for(int i = 0; i < RoomSize; i++)
        {
            mUserList[i] = nullptr;
        }

        RoomLog = new RoomLog[1000];        
        for(int i = 0; i < 1000; i++)
        {
            RoomLog[i] = {nullptr,nullptr};
        }
        
    }
    
    Room::~Room()
    {
        delete[] mUserList;
        delete[] RoomLog;
    }

    void Room::ExitUser(const User* ExitUserName)
    {
        char* ExitUserID;
        ExitUserID = new char[strlen(GetUserID(ExitUserName))-1];
        strcpy(ExitUserID, ExitUserName.GetUserID());

        int flage = 0;
        int tem = 10;

        for(int i = 0; i <= RoomSize - 1; i++)
        {
            tem = strcmp(ExitUserID,mUserList[i].GetUserID);
            if(tem == 0)
            {
                flage = i;
                break;
            }
        }

        if(tem != 0)
        {
            std::cout<<"해당 유저를 발견하지 못했습니다."<<std::endl;
        }
        
        else
        {
            mUserList[flage] = nullptr;

            for(int i=0;i<1000;i++)
            {
                if(RoomLog[i].UserName == nullptr)
                {
                    RoomLog[i] = {ExitUser,"유저가 방을 나갔습니다."};
                }
            }

        }
    }

    void Room::EnterUser(const User* EnterUserName)
    {
        int flage = -1;
        for(int i = 0; i <= RoomSize - 1; i++)
        {
            if(mUserList[i] == nullptr)
            {
                flage = i;
                break;
            }
        }

        if(flage == -1)
        {
            std::cout<<"방이 꽉 찼습니다."<<std::endl;
        }

        else
        {
            mUserList[flage] = EnterUserName;
            for(int i=0;i<1000;i++)
            {
                if(RoomLog[i].UserName == nullptr)
                {
                    RoomLog[i] = {ExitUser,"유저가 방을 나갔습니다."};
                }
            }
            
        }

    }

    void Room:chatting(const User* UserName_,char* speaking)
    {
        if(RoomLog[999].UserName != nullptr)
        {
            std::cout<<"방이 허용이상의 데이터가 들어왔습니다."<<std::endl;
            std::cout<<"새로운 방을 만드세요"<<std::endl;
        }
        else
        {
             for(int i = 0; i < 1000; i++)
             {
                 if(RoomLog[i].UserName == nullptr)
                 {
                     RoomLog[i] = {UserName_,speaking};
                 }
             }
        }
    }

    void Room::chattingLog()
    {
        for(int i = 0; i < 1000; i++)
        {
            if(RoomLog[i].UserName_ == nullptr)
            {
                break;
            }
            std::cout<<RoomLog[i].UserName_<<"-> "<<RoomLog[i].ChatLog <<std::endl;
        }
    }

}
