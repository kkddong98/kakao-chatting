#pragma once
#include<iostrem>
#include<string.h>

namespace Study_1
{
    class User
    {
        public:
        User(char* inputName, char* inputID);//생성자
        ~User();//소멸자
        void ChangeUserName(User* NewName);//이름 바꾸기
        char* GetUserID() const;//id 얻기
        char* GetUserName() const;//유저 이름 얻기

        private:
        char* UserName;
        char* UserID;
    };
}

